<?php


namespace SilverStripe\SMIME\Exceptions;


use Exception;

/**
 * Class InvalidArgumentException
 */
class InvalidArgumentException extends Exception
{

}
